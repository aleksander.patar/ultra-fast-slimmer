import numpy as np
import cv2

# RFB setup
from src_ultra.vision.ssd.config.fd_config import define_img_size
input_img_size = 320
define_img_size(input_img_size)
from src_ultra.vision.ssd.mb_tiny_RFB_fd import create_Mb_Tiny_RFB_fd, create_Mb_Tiny_RFB_fd_predictor
label_path = "./src_ultra/models/voc-model-labels.txt"
class_names = [name.strip() for name in open(label_path).readlines()]
num_classes = len(class_names)
test_device = "cpu"
candidate_size = 10000
threshold = 0.99
model_path = "src_ultra/models/pretrained/version-RFB-320.pth"
# model_path = "src_ultra/models/pretrained/version-RFB-640.pth"
net = create_Mb_Tiny_RFB_fd(len(class_names), is_test=True, device=test_device)
predictor = create_Mb_Tiny_RFB_fd_predictor(net, candidate_size=candidate_size, device=test_device)
net.load(model_path)
#RFB setup end

cap = cv2.VideoCapture(0)


while(True):
    # Capture frame-by-frame
    ret, camframe = cap.read()
    if camframe is None:
        print("camfame is None, end program")
        break

    image = cv2.cvtColor(camframe, cv2.COLOR_BGR2RGB)
    boxes, labels, probs = predictor.predict(image, candidate_size / 2, threshold)

    for i in range(boxes.size(0)):
        box = boxes[i, :]
        label = f" {probs[i]:.2f}"
        box=list(map(int,box))
        cv2.rectangle(camframe, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 4)
        cv2.putText(camframe, label,
            (box[0], box[1] - 10),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,  # font scale
            (0, 0, 255),
            2)  # line type
        
    # Display the resulting frame
    cv2.imshow('frame',camframe)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
import numpy as np
import cv2

# RFB setup
from src_ultra.vision.ssd.config.fd_config import define_img_size
input_img_size = 320
define_img_size(input_img_size)
from src_ultra.vision.ssd.mb_tiny_RFB_fd import create_Mb_Tiny_RFB_fd, create_Mb_Tiny_RFB_fd_predictor
label_path = "./src_ultra/models/voc-model-labels.txt"
class_names = [name.strip() for name in open(label_path).readlines()]
num_classes = len(class_names)
test_device = "cpu"
candidate_size = 10000
threshold = 0.99
model_path = "src_ultra/models/pretrained/version-RFB-320.pth"
# model_path = "src_ultra/models/pretrained/version-RFB-640.pth"
net = create_Mb_Tiny_RFB_fd(len(class_names), is_test=True, device=test_device)
predictor = create_Mb_Tiny_RFB_fd_predictor(net, candidate_size=candidate_size, device=test_device)
net.load(model_path)
#RFB setup end

img = cv2.imread('armand.jpg')
inputimage = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
boxes, labels, probs = predictor.predict(inputimage, candidate_size / 2, threshold)

for i in range(boxes.size(0)):
    box = boxes[i, :]
    label = f" {probs[i]:.2f}"
    box=list(map(int,box))
    cv2.rectangle(img, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 4)

cv2.imshow('image',img)
cv2.waitKey(0)

cv2.imwrite('output.jpg',img)
cv2.destroyAllWindows()